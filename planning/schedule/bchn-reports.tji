# Report definitions for Bitcoin Cash Node
#
# Copyright 2020 freetrader <freetrader@tuta.io>
#
# Adapted from Fedora-20 project plan example
# Copyright 2011 John Poelstra <poelstra@fedoraproject.org>
# Copyright 2011 Robyn Bergeron <rbergero@fedoraproject.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

navigator menu

macro all_navbar [
  header -8<-
== [[File:icons/bchnlogo.png|bottom]] ${project_name_long} - Project Plan v${bchn_project_plan_version} ==
<[navigator id="menu"]>
->8-
]
macro HLPrefix [${project_name_short}]

textreport "Public Reports" {
  formats html
  ${all_navbar}
  timeformat "%a %Y-%m-%d"
  scenarios plan
  start 2020-05-01
  epilog "Epilog is here"

  taskreport "index" {
    title "Overview"
    headline "${HLPrefix} Project Overview"
    columns name, start, end, duration, chart { scale month }
    sorttasks tree, actual.start.up, seqno.up
    hidetask hidden | isleaf()
  }

  taskreport "${project_name_short}-task-detail" {
    title "Details"
    headline "${HLPrefix} Task Detail"
    columns name, duration, chart { scale month }
    sorttasks tree, seqno.up, actual.start.up
    hidetask hidden
  }

  taskreport "${project_name_short}-all-tasks" {
    title "Durations"
    headline "${project_name_short} Task Durations"
    columns name, start, end, duration
    sorttasks tree, actual.start.up, seqno.up
    hidetask hidden | (plan.duration = 0.0)
  }

  taskreport "${project_name_short}-milestones" {
    title "Milestones"
    headline "${project_name_short} Milestones"
    caption "Note: The milestones are currently floating (not fixed, may vary)."
    columns name, start
    sorttasks actual.start.up
    hidetask ~key | (plan.duration > 0.0)
  }

  purge formats
}
